package com.mandala.kyc.Model;
import lombok.ToString;

import javax.persistence.*;

@Embeddable
public class FamilyMembers {




    private String family_name;
    private String relation;
    private String citizen_ship_no;
//    private String issue_district;

    public FamilyMembers() {
    }

    public FamilyMembers(String family_name, String relation, String citizen_ship_no) {
        this.family_name = family_name;
        this.relation = relation;
        this.citizen_ship_no = citizen_ship_no;

    }

    public String getFamily_name() {
        return family_name;
    }

    public void setFamily_name(String family_name) {
        this.family_name = family_name;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public String getCitizen_ship_no() {
        return citizen_ship_no;
    }

    public void setCitizen_ship_no(String citizen_ship_no) {
        this.citizen_ship_no = citizen_ship_no;
    }

//    public String getIssue_district() {
//        return issue_district;
//    }
//
//    public void setIssue_district(String issue_district) {
//        this.issue_district = issue_district;
//    }


}
