package com.mandala.kyc.Model;

import lombok.*;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;

import java.util.*;

@Entity
@Data
@NoArgsConstructor
@ToString
public class FamilyDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

     @ElementCollection()
     @CollectionTable(joinColumns = @JoinColumn(name = "id"))
     @LazyCollection(LazyCollectionOption.FALSE)
     private List<FamilyMembers> family_members;


    @OneToOne(mappedBy = "familyDetails", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private ClientDetails clientDetails;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<FamilyMembers> getFamily_members() {
        return family_members;
    }

    public void setFamily_members(List<FamilyMembers> family_members) {
        this.family_members = family_members;
    }


    public ClientDetails getClientDetails()
    {
        return clientDetails;
    }

    public void setClientDetails(ClientDetails clientDetails)
    {
        this.clientDetails = clientDetails;
   }

}
