package com.mandala.kyc.Model;

import lombok.*;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
@Data
@ToString
public class AssociatedProfession
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String full_name;

    private String code_no;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date date;


    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(joinColumns = @JoinColumn(name = "id"))
    private List<AssociatedProfessionDetails> associatedProfessionDetails;




    @OneToOne(mappedBy= "associatedProfession" ,fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private ClientDetails clientDetails;




    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<AssociatedProfessionDetails> getAssociatedProfessionDetails() {
        return associatedProfessionDetails;
    }

    public void setAssociatedProfessionDetails(List<AssociatedProfessionDetails> associatedProfessionDetails) {
        this.associatedProfessionDetails = associatedProfessionDetails;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getCode_no() {
        return code_no;
    }

    public void setCode_no(String code_no) {
        this.code_no = code_no;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public ClientDetails getClientDetails() {
        return clientDetails;
    }

    public void setClientDetails(ClientDetails clientDetails) {
        this.clientDetails = clientDetails;
    }
}
