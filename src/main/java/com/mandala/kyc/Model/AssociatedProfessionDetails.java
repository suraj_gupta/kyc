package com.mandala.kyc.Model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;

@Embeddable
public class AssociatedProfessionDetails {


    @Column(length = 65535, columnDefinition = "text")
    private String organization_name;

    @Column(length = 65535, columnDefinition = "text")
    private String organization_address;

    private String designation;

    private String expected_annual_income;

    private String  total_expected_annual_income;


    public AssociatedProfessionDetails() {
    }

    public AssociatedProfessionDetails(String organization_name, String organization_address, String designation, String expected_annual_income, String total_expected_annual_income) {
        this.organization_name = organization_name;
        this.organization_address = organization_address;
        this.designation = designation;
        this.expected_annual_income = expected_annual_income;
        this.total_expected_annual_income = total_expected_annual_income;
    }

    public String getOrganization_name() {
        return organization_name;
    }

    public void setOrganization_name(String organization_name) {
        this.organization_name = organization_name;
    }

    public String getOrganization_address() {
        return organization_address;
    }

    public void setOrganization_address(String organization_address) {
        this.organization_address = organization_address;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getExpected_annual_income() {
        return expected_annual_income;
    }

    public void setExpected_annual_income(String expected_annual_income) {
        this.expected_annual_income = expected_annual_income;
    }

    public String getTotal_expected_annual_income() {
        return total_expected_annual_income;
    }

    public void setTotal_expected_annual_income(String total_expected_annual_income) {
        this.total_expected_annual_income = total_expected_annual_income;
    }
}

