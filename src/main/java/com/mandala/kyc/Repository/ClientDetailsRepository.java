package com.mandala.kyc.Repository;

import com.mandala.kyc.Model.ClientDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;
import java.util.Optional;


@Repository
public interface ClientDetailsRepository extends JpaRepository<ClientDetails, Integer> {



    @Query("SELECT t FROM ClientDetails t WHERE t.bank_name ='Agricultural Development Bank Ltd' OR  t.bank_name='Mega Bank Nepal Limited'")
    List<ClientDetails> findTopByOrderByBank_nameDesc();




    @Query("SELECT  t FROM ClientDetails t WHERE t.id=:id  AND t.bank_name=:name")
    Optional<ClientDetails> findTopByOrderByBank_nameAndIdDesc(Integer  id,String name);



    List<ClientDetails> findTopByOrderByIdDesc();


//    @Query("SELECT t FROM ClientDetails t WHERE t.broker_name ='Siprabi' OR t.broker_name='Nalta'")
//    List<ClientDetails> findTopByOrderByBroker_nameDesc();
//
//    @Query("SELECT  t FROM ClientDetails t WHERE t.broker_name=:broker_name AND t.id=:id")
//    Optional<ClientDetails> findTopByOrderByBroker_nameAndIdDesc(Integer id, String broker_name);




}
