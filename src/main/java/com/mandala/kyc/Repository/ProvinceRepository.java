package com.mandala.kyc.Repository;

import com.mandala.kyc.Model.Province;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository

public interface ProvinceRepository extends JpaRepository<Province,Integer> {

}
