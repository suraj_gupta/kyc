package com.mandala.kyc.Repository;

import com.mandala.kyc.Model.Bank;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BankRepository extends JpaRepository<Bank,Integer> {

//     List<Bank> findTopByOrderByIdDesc();

//    String findAllByBank_name(String bank_name);
}
